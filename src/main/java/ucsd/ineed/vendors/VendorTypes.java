package ucsd.ineed.vendors;

import java.util.List;

public class VendorTypes {
	private List<Vendor_type> vendor_types;

	public VendorTypes(){
			
	}
	
	public List<Vendor_type> getVendor_types() {
		return vendor_types;
	}

	public void setVendor_types(List<Vendor_type> vendor_types) {
		this.vendor_types = vendor_types;
	}
}
