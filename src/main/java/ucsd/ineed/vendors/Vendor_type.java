package ucsd.ineed.vendors;

public class Vendor_type {
   String type;
   
   public Vendor_type(){
	   
   }
   
   public Vendor_type(String name) {
	this.type = name;
   }

   public String getType(){
	   return type;
   }
   
   public void setType(String type){
	   this.type = type;
   }
}
