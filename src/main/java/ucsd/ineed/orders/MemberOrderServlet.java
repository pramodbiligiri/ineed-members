package ucsd.ineed.orders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;

import ucsd.ineed.deals.DealCategoriesService;
import ucsd.ineed.members.profile.CategoriesWithChosen;
import ucsd.ineed.members.profile.MemberProfileInfo;
import ucsd.ineed.members.profile.MemberProfileService;
import ucsd.ineed.vendors.Vendor_type;

public class MemberOrderServlet extends VelocityServlet  {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected Template handleRequest(HttpServletRequest req, 
			HttpServletResponse resp, Context ctx) {
		Template template = null;
        HttpSession session = req.getSession();
        String email = session.getAttribute("loggedEmail").toString();
		if("GET".equals(req.getMethod())){
			MemberOrderService orderService = new MemberOrderService();
			Order[] memberOrders = orderService.getMemberOrders(email);
			
			if(memberOrders.length != 0){
				resp.setStatus(HttpServletResponse.SC_OK);
				ctx.put("orders", memberOrders);
				template = Velocity.getTemplate("templates/viewOrders2.vm");
			}
			else{
				resp.setStatus(HttpServletResponse.SC_OK);
				try {
					resp.getWriter().write("<html>"
							+ "<head>"
							+ "<title>Welcome</title>"
							+ "</head>"
							+ "<body>"
							+ "<h3> Error: "+"This member account doesn't seem to exist!"
							+ "<p><a style='color: blue;' href=\"Profile.html\">Go to my profile</a></p>"
							+ "</body>"
							+ "</html>");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
		}
		 return template;
	}
}
