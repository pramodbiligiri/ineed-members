package ucsd.ineed.orders;

import java.util.ArrayList;
import java.util.List;

public class Order {
	 private String orderId;
	 private float total;
	 private String orderState;
	 private float tax;
	 private List<List<Item>> items = new ArrayList<List<Item>>();
	
	 public Order(){
		 
	 }

	public float getTotal() {
		return total;
	}
	public void setTotal(float total) {
		this.total = total;
	}
	public float getTax() {
		return tax;
	}
	public void setTax(float tax) {
		this.tax = tax;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public List<List<Item>> getItems() {
		return items;
	}

	public void setItems(List<List<Item>> items) {
		this.items = items;
	}
	 
}
