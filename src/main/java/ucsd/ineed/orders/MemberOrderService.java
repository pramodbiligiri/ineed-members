package ucsd.ineed.orders;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ucsd.ineed.members.profile.MemberProfileInfo;
import ucsd.ineed.members.profile.creditCard;
import ucsd.ineed.vendors.Vendor;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class MemberOrderService {

	public Order[] getMemberOrders(String email){
		
		/*************hardcoded email*****************/
//		email = "john@example.com";
		/******************************/
		
		//String uri = "https://ineed-db.mybluemix.net/api/orders?memberEmail="+email;
		String uri = "https://orders.mybluemix.net/api/v1/members/"+email+"/orders";
		Order[] orders = null;
		try{
			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = reader.readLine())!=null) {
				sb.append(line);
				sb.append("\n");
			}

			String s = sb.toString();
			ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			orders = objectMapper.readValue(s, Order[].class);
			
			//For testing only: Adding dummy order
//			for (Order order : orders) {
//				if(!order.getItems().isEmpty()){
//					order.getItems().get(0).add(createDummyItem());
//				}
//			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return orders;
	}

	private Item createDummyItem() {
		Item item = new Item();
		item.setDescription("desc1");
		item.setItemID("id1");
		item.setName("Dummy Item1");
		item.setPrice(5.0f);
		item.setQuantity(5);
		item.setVendor(new Vendor());
		
		return item;
	}
	
}
