package ucsd.ineed.members.login;

public class LocalSessionStatus implements SessionCreationStatus {
	
	private boolean isSuccess;

	@Override
	public boolean isSuccess() {
		return isSuccess;
	}

	@Override
	public SessionCreationStatus setSuccess() {
		this.isSuccess = true;
		return this;
	}

	@Override
	public SessionCreationStatus setFailure() {
		this.isSuccess = false;
		return this;
	}

}
