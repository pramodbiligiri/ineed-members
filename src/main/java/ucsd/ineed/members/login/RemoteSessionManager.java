package ucsd.ineed.members.login;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ucsd.ineed.members.config.AppConfig;
import ucsd.ineed.members.util.HttpConnectionUtil;
import static ucsd.ineed.members.util.StringHelpers.*;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class RemoteSessionManager implements SessionManager {

	@Override
	public SessionCreationStatus createSession(String email, HttpServletRequest req, 
			HttpServletResponse resp) {
		SessionCreationStatus sessionStatus = new SessionStatusImpl();
		
		String uri = "http://ineed-db.mybluemix.net/api/sessions";
		try{
			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			SessionEntry sessionEntry = new SessionEntry(email, CookieUtils.generateANewSessionId(), 
					CookieUtils.generateExpirationDate());
			String json = ow.writeValueAsString(sessionEntry);
			OutputStreamWriter wr= new OutputStreamWriter(connection.getOutputStream());
			wr.write(json);
			wr.flush();
			
			int responseCode = connection.getResponseCode();
			if(responseCode == HttpURLConnection.HTTP_OK){
				parseResponseAndSetCookies(req, resp, connection);
				sessionStatus.setSuccess();				
			}
		} catch (Exception e) {
			e.printStackTrace();
			sessionStatus.setFailure();
		}
		return sessionStatus;
	}
	
	@Override
	public SessionCreationStatus deleteSession(HttpServletRequest req, HttpServletResponse resp) {
		Cookie[] cookies = req.getCookies();
	
		req.getSession().invalidate();

		String email = null;
		String sessionToken = null;
		for (Cookie cookie : cookies) {
			if("memberEmail".equals(cookie.getName())){
				email = cookie.getValue();
			}else if("sessionToken".equals(cookie.getName())){
				sessionToken = cookie.getValue();
			}
		}
		
		CookieUtils.CookieDetails cookieDetails = CookieUtils.getCookieDetails(req);
		
		String cookiePath = null;
		String cookieDomain = null;
		if(cookieDetails != null){
			cookiePath = cookieDetails.getPath();
			cookieDomain = cookieDetails.getDomain();
		}
		
		if(isNullOrEmpty(email) || isNullOrEmpty(sessionToken)){
			return new SessionStatusImpl().setSuccess();
		}

		SessionCreationStatus sessionStatus = new SessionStatusImpl();
		try{
			//If no such session exists, just continue, don't indicate anything
			//to user. TODO: pramod: Should add logging indicating a bug or abuse.
			if(!checkIfValidSession(email, sessionToken)){
				return new SessionStatusImpl().setSuccess();
			}
			
			HttpURLConnection connection = deleteRemoteSessionEntry(email, sessionToken);

			int responseCode = connection.getResponseCode();
			if(responseCode == HttpURLConnection.HTTP_OK){
				CookieUtils.deleteCookies(resp, cookieDomain, cookiePath);
				sessionStatus.setSuccess();				
			}else{
				sessionStatus.setFailure();
			}
		} catch (Exception e) {
			e.printStackTrace();
			sessionStatus.setFailure();
		}
		return sessionStatus;
	}

	private HttpURLConnection deleteRemoteSessionEntry(String email,
			String sessionToken) throws MalformedURLException, IOException,
			ProtocolException, JsonProcessingException {
		String uri = "http://ineed-db.mybluemix.net/api/sessions/"+sessionToken;
		URL url = new URL(uri);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		
		connection.setDoOutput(true);
		connection.setRequestMethod("DELETE");
		connection.setRequestProperty("Content-Type", "application/json");
		connection.connect();
		
		return connection;
	}

	private boolean checkIfValidSession(String email, String sessionToken) throws IOException {
		String uri = "http://ineed-db.mybluemix.net/api/sessions/"+sessionToken;
		URL url = new URL(uri);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		
		connection.setDoOutput(true);
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Content-Type", "application/json");
	
		int responseCode = connection.getResponseCode();
		
		if(responseCode == HttpURLConnection.HTTP_OK){
			String responseBody = HttpConnectionUtil.getResponseAsString(connection);
			ObjectMapper objectMapper = new ObjectMapper().configure(
					DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			SessionEntry responseSession = objectMapper.readValue(responseBody, SessionEntry.class);
			return email.equals(responseSession.getMemberEmail());
		}
		
		return false;
	}

	private void parseResponseAndSetCookies(HttpServletRequest req,
			HttpServletResponse resp, HttpURLConnection connection)
			throws IOException, JsonParseException, JsonMappingException {
		String responseBody = HttpConnectionUtil.getResponseAsString(connection);
		ObjectMapper objectMapper = new ObjectMapper().configure(
				DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		SessionEntry responseSession = objectMapper.readValue(responseBody, SessionEntry.class);
		CookieUtils.setCookies(responseSession, req, resp);
		req.getSession().setAttribute("loggedEmail",responseSession.getMemberEmail());
	}
}

