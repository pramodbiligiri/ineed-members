package ucsd.ineed.members.login;

class SessionEntry {
	private String memberEmail;
	private String sessionToken;
	private String expirationDate;
	
	public SessionEntry(){} //To keep Jackson happy

	SessionEntry(String memberEmail, String sessionToken, String expirationDate){
		this.memberEmail = memberEmail;
		this.sessionToken = sessionToken;
		this.expirationDate = expirationDate;
	}

	public String getMemberEmail() {
		return memberEmail;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public String getSessionToken() {
		return sessionToken;
	}
	
}