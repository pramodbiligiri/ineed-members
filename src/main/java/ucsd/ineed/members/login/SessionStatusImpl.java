package ucsd.ineed.members.login;

public class SessionStatusImpl implements SessionCreationStatus {
	
	private boolean isSuccess;

	@Override
	public SessionCreationStatus setSuccess() {
		isSuccess = true;
		return this;
	}
	
	@Override
	public SessionCreationStatus setFailure() {
		isSuccess = false;
		return this;
	}

	@Override
	public boolean isSuccess() {
		return isSuccess;
	}

}