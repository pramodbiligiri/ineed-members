package ucsd.ineed.members.login;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ucsd.ineed.members.config.AppConfig;
import static ucsd.ineed.members.config.AppConfig.SESSION_MANAGER_TYPE;
import static ucsd.ineed.members.config.AppConfig.REMOTE;
import ucsd.ineed.members.profile.MemberProfileInfo;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LoginService {

	private final SessionManager sessionManager;
	
	public LoginService(){
		if(REMOTE.equalsIgnoreCase(AppConfig.get(SESSION_MANAGER_TYPE))){
			sessionManager = new RemoteSessionManager();
		}else{
			sessionManager = new LocalSessionManager();
		}
	}
	
	public MemberProfileInfo[] getMemberByEmail(String email) {
		MemberProfileInfo[] member = null;
		String uri = "http://ineed-db.mybluemix.net/api/members/?email="+email;
		
		try{
			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = reader.readLine())!=null) {
				sb.append(line);
				sb.append("\n");
			}

			String s = sb.toString();
			ObjectMapper objectMapper = new ObjectMapper().configure(
					DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			member = objectMapper.readValue(s, MemberProfileInfo[].class);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return member;
	}

	public SessionCreationStatus createSession(String email, HttpServletRequest req, 
			HttpServletResponse resp) {
		return sessionManager.createSession(email, req, resp);
	}
	
	public SessionCreationStatus deleteSession(HttpServletRequest req, 
			HttpServletResponse resp) {
		return sessionManager.deleteSession(req, resp);
	}

}

