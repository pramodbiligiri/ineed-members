package ucsd.ineed.members.login;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ucsd.ineed.members.login.CookieUtils.CookieDetails;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class LocalSessionManager implements SessionManager {

	@Override
	public SessionCreationStatus createSession(String email, HttpServletRequest req, 
			HttpServletResponse resp) {
		HttpSession session = req.getSession();
        session.setAttribute("loggedEmail",email);
        
        try {
			CookieUtils.setCookies(
					new SessionEntry(email, CookieUtils.generateANewSessionId(), 
						CookieUtils.generateExpirationDate()), req, resp);
			return new SessionStatusImpl().setSuccess();
		} catch (JsonParseException e) {
			e.printStackTrace();
			return new SessionStatusImpl().setFailure();
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return new SessionStatusImpl().setFailure();
		} catch (IOException e) {
			e.printStackTrace();
			return new SessionStatusImpl().setFailure();
		}
	}

	@Override
	public SessionCreationStatus deleteSession(HttpServletRequest req,
			HttpServletResponse resp) {
		try{
			CookieDetails cookieDetails = CookieUtils.getCookieDetails(req);
			if(cookieDetails != null){
				CookieUtils.deleteCookies(resp, cookieDetails.getDomain(), 
						cookieDetails.getPath());
			}
			req.getSession().invalidate();
		}catch(IllegalStateException e){
			e.printStackTrace();
		}
		return new SessionStatusImpl().setSuccess();
	}
}
