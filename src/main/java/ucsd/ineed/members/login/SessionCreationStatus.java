package ucsd.ineed.members.login;

public interface SessionCreationStatus {

	boolean isSuccess();
	SessionCreationStatus setSuccess();
	SessionCreationStatus setFailure();

}