package ucsd.ineed.members.login;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ucsd.ineed.members.config.AppConfig;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class CookieUtils {
	
	public static void setCookies(SessionEntry sessionEntry, HttpServletRequest req,
			HttpServletResponse resp)
			throws IOException, JsonParseException, JsonMappingException {
		resp.addCookie(enableCrossDomainIfNecessary(req, 
		    		new Cookie("memberEmail", sessionEntry.getMemberEmail())));
		resp.addCookie(enableCrossDomainIfNecessary(req, 
					new Cookie("expirationDate", sessionEntry.getExpirationDate())));
		resp.addCookie(enableCrossDomainIfNecessary(req, 
					new Cookie("sessionToken", sessionEntry.getSessionToken())));
	}
	
	static String generateANewSessionId() {
		return java.util.UUID.randomUUID().toString();
	}
	
	static String generateExpirationDate(){
		Calendar c = Calendar.getInstance(); 
		c.setTime(new Date()); 
		c.add(Calendar.DATE, Integer.parseInt(AppConfig.get(AppConfig.LOGIN_SESSION_DURATION)));
		String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
		return dateStr;
	}
	
	private static Cookie enableCrossDomainIfNecessary(HttpServletRequest req, Cookie cookie) {
		if(req.getServerName() != null && req.getServerName().contains("mybluemix.net")){
			cookie.setDomain(".mybluemix.net");
		}
		return cookie;
	}

	static void deleteCookies(HttpServletResponse resp, String cookieDomain,
			String cookiePath) {
		String[] cookies = {"memberEmail", "sessionToken", "expirationDate"};
		for (String cookieName : cookies) {
			Cookie cookie = new Cookie(cookieName, "");
			cookie.setMaxAge(0);
			if(cookieDomain != null)
				cookie.setDomain(cookieDomain);
			if(cookiePath != null)
				cookie.setPath(cookiePath);
			resp.addCookie(cookie);
		}
	}
	
	public static class CookieDetails {
		private final String domain;
		private final String path;

		public CookieDetails(String domain, String path){
			this.domain = domain;
			this.path = path;
		}

		public String getDomain() {
			return domain;
		}

		public String getPath() {
			return path;
		}
	}

	static CookieDetails getCookieDetails(HttpServletRequest req) {
		CookieDetails cookieDetails = null;
		for (Cookie cookie : req.getCookies()) {
			if("memberEmail".equals(cookie.getName())){
				cookieDetails = new CookieDetails(
						cookie.getPath(), cookie.getDomain());
			}
		}
		return cookieDetails;
	}

}
