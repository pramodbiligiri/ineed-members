package ucsd.ineed.members.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SessionManager {

	SessionCreationStatus createSession(String email, HttpServletRequest req,
			HttpServletResponse resp);

	SessionCreationStatus deleteSession(HttpServletRequest req,
			HttpServletResponse resp);

}