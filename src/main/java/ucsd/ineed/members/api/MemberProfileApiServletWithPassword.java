package ucsd.ineed.members.api;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ucsd.ineed.members.profile.MemberProfileInfo;
import ucsd.ineed.members.profile.MemberProfileService;
import ucsd.ineed.members.register.MemberRegistrationCandidate;
import ucsd.ineed.members.register.PasswordHash;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

public class MemberProfileApiServletWithPassword extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		MemberProfileInfo[] profiles = new MemberProfileService().getMemberProfile(email);
		if(profiles.length == 0){
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.setContentType("text/json");
			resp.getWriter().write("{}");
			resp.flushBuffer();
			return;
		}else{
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.setContentType("text/json");
			MemberProfileInfo profile = profiles[0];
			byte[] hashedPassword = null;
			try {
				hashedPassword = PasswordHash.getHashedPassword(password);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			if(Arrays.equals(hashedPassword, profile.getHashedPassword())){
				resp.getWriter().write(new ObjectMapper().writeValueAsString(profile));
				resp.flushBuffer();
			}
			
			return;
		}
	}
}
