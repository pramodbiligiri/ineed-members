package ucsd.ineed.members.api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ucsd.ineed.members.profile.MemberProfileInfo;
import ucsd.ineed.members.profile.MemberProfileService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

public class MemberProfileApiServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String email = req.getParameter("email");
		MemberProfileInfo[] profiles = new MemberProfileService().getMemberProfile(email);
		if(profiles.length == 0){
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.setContentType("text/json");
			resp.getWriter().write("{}");
			resp.flushBuffer();
			return;
		}else{
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.setContentType("text/json");
			MemberProfileInfo profile = profiles[0];
			profile.setHashedPassword("******".getBytes("UTF-8"));
			
			//TODO: The filtering of password field isn't working.
			SimpleFilterProvider filters = new SimpleFilterProvider();
			filters.setFailOnUnknownId(false);
			filters.setDefaultFilter(SimpleBeanPropertyFilter.serializeAllExcept("hashedPassword"));

			resp.getWriter().write(new ObjectMapper().writer(filters).writeValueAsString(profile));
			resp.flushBuffer();
			
			return;
		}
	}
}
