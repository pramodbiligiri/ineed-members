package ucsd.ineed.members.config;

import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ConfigInitializer implements ServletContextListener{

	@Override
	public void contextDestroyed(ServletContextEvent event) {
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		try {
			AppConfig.init(event.getServletContext());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
