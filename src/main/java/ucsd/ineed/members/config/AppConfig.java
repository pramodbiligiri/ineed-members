package ucsd.ineed.members.config;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContext;

public class AppConfig {
	
	public static final String LOGIN_SESSION_DURATION = "login.session.duration";

	//Enforce Singleton
	private AppConfig(){}

	private static Properties config;
	
	synchronized static void init(ServletContext servletContext) throws IOException{
		if(config == null){
			config = new Properties();
			config.load(servletContext.getResourceAsStream("/WEB-INF/lib/ineed-members.conf"));
		}
	}
	
	public static String get(String key){
		return config.getProperty(key);
	}
	
	//Define string constants for various properties
	public static final String REMOTE = "remote";
	public static final String SESSION_MANAGER_TYPE = "session.manager.type";
}
