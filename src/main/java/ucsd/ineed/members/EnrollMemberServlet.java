package ucsd.ineed.members;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ucsd.ineed.members.register.MemberRegistrationCandidate;
import ucsd.ineed.members.register.MemberRegistrationService;
import ucsd.ineed.members.register.RegistrationStatus;
import ucsd.ineed.members.register.RemoteRegistrationService;

public class EnrollMemberServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		MemberRegistrationCandidate member = null;
		RegistrationStatus registrationStatus = new RegistrationStatus();
		registrationStatus.setSuccess();
		
		try {
			member = MemberRegistrationCandidate.fromHttpRequest(req);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			registrationStatus.setMessage("Internal server error");
			registrationStatus.setFailure();
		}
		
		if(registrationStatus.isSuccess()){
			MemberRegistrationService registrationService = new MemberRegistrationService(
					new RemoteRegistrationService());
			registrationStatus = registrationService.registerMember(member);
		}
		
		if (registrationStatus.isSuccess()) {
			resp.setStatus(HttpServletResponse.SC_OK);
			/*
			resp.getWriter().write("<html>"
					+ "<head>"
					+ "<title>Enrollment</title>"
					+ "</head>"
					+ "<body>"
					+ "<h3>Hello "+ req.getParameter("firstName")+", you have been enrolled successfully."
					+ "<p><a style='color: blue;' href=\"index_vanilla_bootstrap2.html\">Login now</a></p>"
					+ "</body>"
					+ "</html>");
			*/
			req.getSession().setAttribute("loggedEmail", req.getParameter("email"));
			resp.sendRedirect("memberOptions2.html");
			
		} else {
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.getWriter().write("<html>"
					+ "<head>"
					+ "<title>Welcome</title>"
					+ "</head>"
					+ "<body>"
					+ "<h3>Either this email is already in use or some error occured. Try again."
					+ "<p><a style='color: blue;' href=\"index_vanilla_bootstrap2.html\">Enroll</a></p>"
					+ "</body>"
					+ "</html>");
		}
		resp.flushBuffer();
	}
}
