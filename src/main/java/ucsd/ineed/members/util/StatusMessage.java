package ucsd.ineed.members.util;

public class StatusMessage {

	public static final StatusMessage SUCCESS_DEFAULT = new StatusMessage("Success").setSuccess();
	
	private final String message;
	private boolean success;

	public StatusMessage(String message) {
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}

	public StatusMessage setFailure() {
		this.success = false;
		return this;
	}
	
	public StatusMessage setSuccess() {
		this.success = true;
		return this;
	}
	
	public boolean isSuccess(){
		return success;
	}

}
