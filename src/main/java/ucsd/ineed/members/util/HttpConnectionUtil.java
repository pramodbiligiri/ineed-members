package ucsd.ineed.members.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class HttpConnectionUtil {
	
	public static String getResponseAsString(HttpURLConnection connection) throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = reader.readLine())!=null) {
			sb.append(line);
			sb.append("\n");
		}
		return sb.toString();
	}
}
