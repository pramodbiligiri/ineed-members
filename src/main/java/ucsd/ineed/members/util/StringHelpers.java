package ucsd.ineed.members.util;

public class StringHelpers {
	
	public static boolean isNullOrEmpty(String str){
		return str == null || str.isEmpty();
	}

}
