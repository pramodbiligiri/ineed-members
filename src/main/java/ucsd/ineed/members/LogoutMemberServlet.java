package ucsd.ineed.members;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ucsd.ineed.members.login.LoginService;

public class LogoutMemberServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private final LoginService loginService = new LoginService();
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		loginService.deleteSession(req, res);
		res.sendRedirect(req.getContextPath());
	}
	
}
