package ucsd.ineed.members;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ucsd.ineed.members.login.LoginService;
import ucsd.ineed.members.login.SessionCreationStatus;
import ucsd.ineed.members.profile.MemberProfileInfo;
import ucsd.ineed.members.register.PasswordHash;
import ucsd.ineed.members.util.StringHelpers;

public class LoginMemberServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private final LoginService loginService =  new LoginService();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		String redirectUrl = req.getParameter("redirectUrl");
		
		if(email == null || email.isEmpty() || password == null || password.isEmpty()){
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.getWriter().write("<html>"
					+ "<head>"
					+ "<title>Welcome</title>"
					+ "</head>"
					+ "<body>"
					+ "<h3>Invalid email or password. Try again"
					+ "<p><a style='color: blue;' href=\"index_vanilla_bootstrap2.html\">Login</a></p>"
					+ "</body>"
					+ "</html>");
			resp.flushBuffer();
			return;
		}
		
		byte[] hashedPassword = null;
		try {
			hashedPassword = PasswordHash.getHashedPassword(password);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		if(hashedPassword == null){
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			resp.getWriter().write("<html><head><title>iNeed</title></head>"+
			"<body><h1>Oops. Internal error. Try again.</h1></body></html>");
			resp.flushBuffer();
			return;
		}
		
		MemberProfileInfo[] members = loginService.getMemberByEmail(email);
		if(members.length != 0){
			MemberProfileInfo member = members[0];
			if(!Arrays.equals(member.getHashedPassword(), hashedPassword)){
				resp.setStatus(HttpServletResponse.SC_OK);
				resp.getWriter().write("<html>"
						+ "<head>"
						+ "<title>Welcome</title>"
						+ "</head>"
						+ "<body>"
						+ "<h3>Invalid email or password. Try again"
						+ "<p><a style='color: blue;' href=\"index_vanilla_bootstrap2.html\">Login</a></p>"
						+ "</body>"
						+ "</html>");
				resp.flushBuffer();
				return;
			}
			
	        SessionCreationStatus sessionStatus = loginService.createSession(email, req, resp);
	        if(sessionStatus.isSuccess()){
	        	if(!StringHelpers.isNullOrEmpty(redirectUrl)){
	        		String actualUrl = URLDecoder.decode(redirectUrl, "UTF-8");
	        		resp.sendRedirect(actualUrl);
	        	}else{
	        		resp.sendRedirect("memberOptions2.html");
	        	}
	        }else{
	        	resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Something went wrong. Sorry, try again");
	        }
		}else{
			resp.getWriter().write("<html>"
					+ "<head>"
					+ "<title>Welcome</title>"
					+ "</head>"
					+ "<body>"
					+ "<h3>Looks like you are not enrolled yet."
					+ "<p><a style='color: blue;' href=\"index_vanilla_bootstrap2.html\">Enroll</a></p>"
					+ "</body>"
					+ "</html>");
		}
		resp.setStatus(HttpServletResponse.SC_OK);		
		resp.flushBuffer();
	}

}
