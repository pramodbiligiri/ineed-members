package ucsd.ineed.members.register;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import ucsd.ineed.members.login.LoginService;
import ucsd.ineed.members.profile.MemberProfileInfo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class RemoteRegistrationService implements MemberRegistration {
	
	private final LoginService loginService = new LoginService();

	@Override
	public RegistrationStatus registerMember(MemberRegistrationCandidate member) {
		RegistrationStatus rs = new RegistrationStatus();
		MemberProfileInfo[] existingMember = loginService.getMemberByEmail(member.getEmail());
		if(existingMember.length!=0){
			rs.setFailure();
			return rs;
		}
		
		String uri = "http://ineed-db.mybluemix.net/api/members";
		try{
			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			String json = ow.writeValueAsString(member);
			OutputStreamWriter wr= new OutputStreamWriter(connection.getOutputStream());
			wr.write(json);
			wr.flush();
			
			int responseCode = connection.getResponseCode();
			if(responseCode == connection.HTTP_OK){
				rs.setSuccess();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rs.setFailure();
		}
		return rs;
	}
		
}
