package ucsd.ineed.members.register;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class MemberRegistrationCandidate {

	private String firstName;
	private String lastName;
	private String email;
	private String mobileNumber;
	private byte[] hashedPassword;
	private List<String> preferences;

	public MemberRegistrationCandidate(String firstName, String lastName, String email, String mobileNumber, byte[] hashedPassword) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.hashedPassword = hashedPassword;
		//Hardcoding one of the preferences here so that some deals can be recommended until the member updates his preferences
		this.setPreferences(Arrays.asList("Restaurant"));
	}

	public static MemberRegistrationCandidate fromHttpRequest(HttpServletRequest req) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");
		String email = req.getParameter("email");
		String mobileNumber = req.getParameter("mobileNumber");
		String password = req.getParameter("password");
		String confirmPassword = req.getParameter("confirmpassword");
		return createMember(firstName, lastName, email, mobileNumber, password, confirmPassword);
	}

	private static MemberRegistrationCandidate createMember(String firstName, String lastName, String email, String mobileNumber, String password, 
			String confirmPassword) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		if(firstName == null || firstName.isEmpty())
			throw new IllegalArgumentException("Member firstName is empty");
		
		if(lastName == null || lastName.isEmpty())
			throw new IllegalArgumentException("Member lastName is empty");
		
		if(email == null || email.isEmpty())
			throw new IllegalArgumentException("Member email is empty");
		
		if(mobileNumber == null || mobileNumber.isEmpty())
			throw new IllegalArgumentException("Member mobileNumber is empty");
		
		if(!password.equals(confirmPassword))
			throw new IllegalArgumentException("Password and Confirm Password fields don't match");
		
		byte[] hashedPassword = PasswordHash.getHashedPassword(password);
		
		return new MemberRegistrationCandidate(firstName, lastName, email, mobileNumber, hashedPassword);
	}

	public boolean conflictsRegistration(MemberRegistrationCandidate member) {
		return email.equals(member.email);
	}
	
	public byte[] getHashedPassword() {
		return this.hashedPassword;
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public String getMobileNumber(){
		return mobileNumber;
	}
	
	public String getEmail(){
		return email;
	}

	public List<String> getPreferences() {
		return preferences;
	}

	public void setPreferences(List<String> preferences) {
		this.preferences = preferences;
	}
}
