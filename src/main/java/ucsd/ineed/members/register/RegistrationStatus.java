package ucsd.ineed.members.register;

public class RegistrationStatus {

	private boolean success;
	private String message;
	
	public void setMessage(String message) {
		this.message = message;
	}

	public RegistrationStatus(){
		
	}

	public RegistrationStatus(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public RegistrationStatus setSuccess() {
		this.success = true;
		return this;
	}

	public RegistrationStatus setFailure() {
		this.success = false;
		return this;
	}
	
	public String getMessage(){
		return message;
	}
	
}
