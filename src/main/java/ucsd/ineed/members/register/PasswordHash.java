package ucsd.ineed.members.register;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordHash {

	public static byte[] getHashedPassword(String password)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digest = MessageDigest.getInstance("SHA");
		byte[] hashedPassword = digest.digest(password.getBytes("UTF-8"));
		return hashedPassword;
	}

}
