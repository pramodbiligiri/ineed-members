package ucsd.ineed.members.register;


public class MemberRegistrationService {

	private MemberRegistration registrationService;

	public MemberRegistrationService(MemberRegistration registrationService) {
		this.registrationService = registrationService;
	}

	public RegistrationStatus registerMember(MemberRegistrationCandidate member) {
		return registrationService.registerMember(member);
	}
}
