package ucsd.ineed.members.register;


public interface MemberRegistration {
	
	public RegistrationStatus registerMember(MemberRegistrationCandidate member);

}
