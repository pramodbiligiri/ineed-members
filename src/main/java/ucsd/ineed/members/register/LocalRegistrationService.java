package ucsd.ineed.members.register;

import java.util.ArrayList;
import java.util.List;

import ucsd.ineed.members.util.StatusMessage;

public class LocalRegistrationService implements MemberRegistration {
	
	private static List<MemberRegistrationCandidate> members = new ArrayList<MemberRegistrationCandidate>();

	@Override
	public RegistrationStatus registerMember(MemberRegistrationCandidate member) {
		StatusMessage statusMessage = validateMemberFieldsForCorrectness(member);
		
		if(!statusMessage.isSuccess()){
			return new RegistrationStatus(statusMessage.getMessage()).setFailure();
		}
		
		for (MemberRegistrationCandidate existingMember : members) {
			if(existingMember.conflictsRegistration(member)){
				return new RegistrationStatus("Email id already taken").setFailure();
			}
		}
		
		members.add(member);
		return new RegistrationStatus().setSuccess();
	}

	private StatusMessage validateMemberFieldsForCorrectness(MemberRegistrationCandidate member) {
//		String password = member.getPassword();
//		String confirmPassword = member.getConfirmPassword();
//		
//		if((password == null || password.isEmpty()) &&
//				(confirmPassword == null || confirmPassword.isEmpty())){
//			return new StatusMessage("Password cannot be empty").setFailure();
//		}
//		
//		if(!password.equals(confirmPassword))
//			return new StatusMessage("Passwords do not match").setFailure();
		
		return StatusMessage.SUCCESS_DEFAULT;
	}

}
