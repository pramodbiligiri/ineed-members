package ucsd.ineed.members;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;

import ucsd.ineed.members.util.StringHelpers;

public class LoginAndRedirectServlet extends VelocityServlet {
	
	@Override
	protected Template handleRequest(HttpServletRequest request,
			HttpServletResponse response, Context ctx)  {
		String redirectUrl = request.getParameter("redirectUrl");
		String formUrl = null;
		
		if(!StringHelpers.isNullOrEmpty(redirectUrl)){
			try {
				formUrl = URLEncoder.encode(redirectUrl, "UTF-8");
				ctx.put("redirectUrl", formUrl);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return Velocity.getTemplate("templates/auth.vm");
	}

}
