package ucsd.ineed.members.profile;

public class creditCard {
	private String cardNumber;
	private String cardCVC2;
	private String cardExpiration;
	
	public creditCard(){
		
	}
	
	public creditCard(String cardNumber, String cardCVC2, String cardExpiration){
		this.cardNumber = cardNumber;
		this.cardCVC2 = cardCVC2;
		this.cardExpiration = cardExpiration;
	}
	
	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardCVC2() {
		return cardCVC2;
	}

	public void setCardCVC2(String cardCVC2) {
		this.cardCVC2 = cardCVC2;
	}

	public String getCardExpiration() {
		return cardExpiration;
	}

	public void setCardExpiration(String cardExpiration) {
		this.cardExpiration = cardExpiration;
	}
}
