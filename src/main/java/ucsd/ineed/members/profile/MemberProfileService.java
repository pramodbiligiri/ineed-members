package ucsd.ineed.members.profile;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ucsd.ineed.members.util.HttpConnectionUtil;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class MemberProfileService {

	public MemberProfileInfo[] getMemberProfile(String email){
		String uri = "http://ineed-db.mybluemix.net/api/members/?email="+email;
		MemberProfileInfo[] members = null;
		try{
			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
			String s = HttpConnectionUtil.getResponseAsString(connection);
			ObjectMapper objectMapper = new ObjectMapper().configure(
					DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			members = objectMapper.readValue(s, MemberProfileInfo[].class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return members;
	}

	public boolean setMemberProfile(MemberProfileInfo memberProfile){
		boolean success = false;
		String email = memberProfile.getEmail();
		String uri = "http://ineed-db.mybluemix.net/api/members/"+email;
		try{
			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("PUT");
			connection.setRequestProperty("Content-Type", "application/json");
			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			String json = ow.writeValueAsString(memberProfile);
			OutputStreamWriter wr= new OutputStreamWriter(connection.getOutputStream());
			wr.write(json);
			wr.flush();
			
			int responseCode = connection.getResponseCode();
			if(responseCode == connection.HTTP_OK){
				success = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
		}
		return success;
	}
	
	public static MemberProfileInfo fromHttp(HttpServletRequest req){
		MemberProfileInfo memberProfile;
		String firstName = req.getParameter("firstname");
		String lastName = req.getParameter("lastname");
		String mobileNumber = req.getParameter("phonenumber");
		String cardNumber = req.getParameter("cardnumber");
		String proximityPreference = req.getParameter("proximitypreference");
		String cardCVC2 = req.getParameter("cardcvc2");
		String cardExpiration = req.getParameter("cardexpiration");
		creditCard cardDetails = new creditCard(cardNumber,cardCVC2,cardExpiration);
		HttpSession session = req.getSession();
		String email = session.getAttribute("loggedEmail").toString();
		memberProfile = new MemberProfileInfo(firstName,lastName,email,mobileNumber,proximityPreference);
		memberProfile.setHashedPassword((byte[])session.getAttribute("hashedPassword"));
		memberProfile.setCreditCard(cardDetails);
		try{
			List<String> preferences = Arrays.asList(req.getParameterValues("preference"));
		    memberProfile.setCategoryPreferences(preferences);
		}catch(NullPointerException e){
			e.printStackTrace();
		}
		return memberProfile;
	}
}
