package ucsd.ineed.members.profile;

public class CategoriesWithChosen {

	private boolean isChosen;
	private String category;
	

	public CategoriesWithChosen(String category) {
		this.category = category;
		this.isChosen = false;
	}

	public void setChosen(boolean isChosen) {
		this.isChosen = isChosen;
	}

	public String getCategory() {
		return category;
	}

	public boolean getIsChosen() {
		return isChosen;
	}
	
}
