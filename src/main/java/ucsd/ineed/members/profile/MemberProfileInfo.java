package ucsd.ineed.members.profile;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class MemberProfileInfo {
	
	private String firstName;
	private String lastName;
	private String email;
	private String mobileNumber;
	private String proximityPreference;
	private byte[] hashedPassword;
	
	//@JsonInclude(JsonInclude.Include.NON_NULL)
	private creditCard creditCard;
	
	
	public byte[] getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(byte[] hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	List<String> categoryPreferences = new ArrayList<String>();
	
	public MemberProfileInfo() {
	}

	public MemberProfileInfo(String firstName, String lastName, String email, String mobileNumber, String proximityPreference) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.proximityPreference = proximityPreference;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}
	
	public void setPreferences(List<String> preferences){
		this.categoryPreferences = preferences;
	}
	
	public void setCategoryPreferences(List<String> preferences){
		this.categoryPreferences = preferences;
	}
	
	public List<String> getCategoryPreferences(){
		return categoryPreferences;
	}
	
	public String getProximityPreference() {
		return proximityPreference;
	}

	public void setProximityPreference(String proximityPreference) {
		this.proximityPreference = proximityPreference;
	}

	public creditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(creditCard creditCard) {
		this.creditCard = creditCard;
	}

	
}
