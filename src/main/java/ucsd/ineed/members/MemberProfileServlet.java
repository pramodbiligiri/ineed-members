package ucsd.ineed.members;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;

import ucsd.ineed.deals.DealCategoriesService;
import ucsd.ineed.members.profile.CategoriesWithChosen;
import ucsd.ineed.members.profile.MemberProfileInfo;
import ucsd.ineed.members.profile.MemberProfileService;
import ucsd.ineed.vendors.Vendor_type;

public class MemberProfileServlet extends VelocityServlet  {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected Template handleRequest(HttpServletRequest req, 
			HttpServletResponse resp, Context ctx) {
		Template template = null;
        HttpSession session = req.getSession();
        String email = session.getAttribute("loggedEmail").toString();
		if("GET".equals(req.getMethod())){
			MemberProfileService profileService = new MemberProfileService();
			List<Vendor_type> allDealCategories = new DealCategoriesService().getAllDealCategories();
            
			MemberProfileInfo[] memberProfile = profileService.getMemberProfile(email);
			
			if(memberProfile.length != 0){
				resp.setStatus(HttpServletResponse.SC_OK);
				MemberProfileInfo memberProfileInfo = memberProfile[0];
				session.setAttribute("hashedPassword", memberProfileInfo.getHashedPassword());
				
				ctx.put("profile", memberProfileInfo);
				ctx.put("categoriesWithChosen", getCategoriesWithChosen(allDealCategories, 
						memberProfileInfo.getCategoryPreferences()));
				ctx.put("carddetails",memberProfileInfo.getCreditCard());
				ctx.put("msg", req.getParameter("msg"));
				template = Velocity.getTemplate("templates/updateprofile2.vm");
			}
			else{
				resp.setStatus(HttpServletResponse.SC_OK);
				try {
					resp.getWriter().write("<html>"
							+ "<head>"
							+ "<title>Welcome</title>"
							+ "</head>"
							+ "<body>"
							+ "<h3> Error: "+"This member account doesn't seem to exist!"
							+ "<p><a style='color: blue;' href=\"Profile.html\">Go to my profile</a></p>"
							+ "</body>"
							+ "</html>");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}else if("POST".equals(req.getMethod())){

			MemberProfileInfo memberProfile  = MemberProfileService.fromHttp(req);

			MemberProfileService profileService = new MemberProfileService();

			boolean status = profileService.setMemberProfile(memberProfile);

			if (status==true) {
				try {
					resp.sendRedirect("updateprofile?msg=Profile+has+been+updated");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				resp.setStatus(HttpServletResponse.SC_OK);
				try {
					resp.getWriter().write("<html>"
							+ "<head>"
							+ "<title>Welcome</title>"
							+ "</head>"
							+ "<body>"
							+ "<h3> Error: "+"Something went wrong.."+". Try again."
							+ "<p><a style='color: blue;' href=\"Profile.html\">Go to my profile</a></p>"
							+ "</body>"
							+ "</html>");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return template;
	}

	private List<CategoriesWithChosen> getCategoriesWithChosen(List<Vendor_type> allDealCategories,
			List<String> preferences) {
		List<CategoriesWithChosen> categoriesWithChosen = new ArrayList<CategoriesWithChosen>();
		
		for (Vendor_type vendor : allDealCategories) {
			String category = vendor.getType();
			CategoriesWithChosen result = new CategoriesWithChosen(category);
			if(preferences.contains(category)){
				result.setChosen(true);
			}
			categoriesWithChosen.add(result);
		}
		return categoriesWithChosen;
	}
}
