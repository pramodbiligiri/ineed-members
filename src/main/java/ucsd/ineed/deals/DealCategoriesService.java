package ucsd.ineed.deals;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ucsd.ineed.members.profile.MemberProfileInfo;
import ucsd.ineed.vendors.VendorTypes;
import ucsd.ineed.vendors.Vendor_type;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DealCategoriesService {
	
	public List<Vendor_type> getAllDealCategories() {
   
        String uri = "http://ineedvendors.mybluemix.net/api/vendor/types";
        
        VendorTypes vendorTypes = null;
		
		try{

			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");

			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = reader.readLine())!=null) {
				sb.append(line);
				sb.append("\n");
			}

			String s = sb.toString();
			ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			vendorTypes = objectMapper.readValue(s, VendorTypes.class);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		return vendorTypes.getVendor_types();
		
		/*
		List<Vendor_type> types = new ArrayList<Vendor_type>();
		types.add(new Vendor_type("movies"));
		types.add(new Vendor_type("concerts"));
		types.add(new Vendor_type("books"));
		types.add(new Vendor_type("food"));
		types.add(new Vendor_type("restaurants"));
		types.add(new Vendor_type("fineDining"));
		
		return types;
		*/
	}
}
