package ucsd.ineed.members.register;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;

import org.junit.Before;
import org.junit.Test;

public class MemberRegistrationTest {

	private MemberRegistration registrationService;

	@Before
	public void setUp(){
		registrationService = new LocalRegistrationService();
	}
	
	@Test
	public void testRegistrationFailsIfPasswordEmpty() throws UnsupportedEncodingException {
		RegistrationStatus status = registrationService.registerMember(
				new MemberRegistrationCandidate("TestUser","last", "test@gmail.com","4243", "password".getBytes("UTF-8")));
		assertEquals(false, status.isSuccess());
	}
	
//	@Test
//	public void testRegistrationFailsIfPasswordsDontMatch() {
//		RegistrationStatus status = registrationService.registerMember(
//				new MemberRegistrationCandidate("TestUser","lastname", "test@gmail.com","6762","password".getBytes("UTF-8"), "abcde".getBytes("UTF-8")));
//		assertEquals(false, status.isSuccess());
//	}
	
	@Test
	public void testRegistrationFailsIfMemberAlreadyExists() throws UnsupportedEncodingException {
		RegistrationStatus status = registrationService.registerMember(
				new MemberRegistrationCandidate("some","thing", "someone@gmail.com","3436", "password".getBytes("UTF-8")));
		assertEquals(true, status.isSuccess());
		
		RegistrationStatus status2 = registrationService.registerMember(
				new MemberRegistrationCandidate("some","thing", "someone@gmail.com","3436", "password".getBytes("UTF-8")));
		assertEquals(false, status2.isSuccess());
	}

}
